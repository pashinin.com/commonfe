// const localHosts = [
//   'localhost',
//   '10.254.239.2',  // my machine (to test using wifi)
// ];

const BROWSER = typeof document !== 'undefined';
const NODEJS = !BROWSER;
// const onLocalhost = localHosts.includes(BROWSER ? document.location.hostname : '');

// This function returns a hostname where API is running
// Example:
//   'localhost:9090' or '10.254.239.2:9090'  when running locally
//   'api.pashinin.com' when running in PROD
//   'api-staging.pashinin.com'
// function apiHost(): string {
//   const hostname = BROWSER ? document!.location.hostname : 'localhost';
//   const staging = hostname.includes('-staging.') || hostname === 'canary.pashinin.com';
//   let host = onLocalhost ? `${hostname}:9090` : 'api.pashinin.com';
//   host = staging ? 'api-staging.pashinin.com' : host;
//   return host;
// }

// Returns a HTTP or HTTPS host where API is running
// Example:
//   'http://localhost:9090'
//   'https://api.pashinin.com'
// function apiHttpHost(): string {
//   const host = apiHost();
//   return onLocalhost ? `http://${host}` : `https://${host}`;
// }

export {
  NODEJS,
  // onLocalhost,
  // apiHost,
  // apiHttpHost,
};
