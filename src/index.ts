// import TopMenu from './TopMenu.vue';

// export TopMenu;
// import { RouteConfig } from 'vue-router';
// import VuetifyToast from 'vuetify-toast-snackbar';

// import Vue from 'vue';
import AppUpdate from './AppUpdate.vue';
import API from './api';
import User from './store/modules/user';
import Response from './response';
import App from './store/modules/app';
// import Login from './Login.vue';
import VueTimeago from 'vue-timeago';
import InfiniteLoading from 'vue-infinite-loading';
// Vue.use(InfiniteLoading, { /* options */ });

import VuetifyToast from './toast.js';


// const routes = [
//   { path: '/login', component: () => import('./Login.vue') },
//   { path: '/profile', component: () => import('./Profile.vue') },
// ];
// as RouteConfig[]

export {
  // routes,
  API,
  Response,
  AppUpdate,

  VuetifyToast,

  App,
  User,
};

const commonfe = {
  install(Vue, options) {
    Vue.use(InfiniteLoading, { /* options */ });

    // Vue.use(VuetifyToast, {
    //   x: 'right', // default
    //   y: 'bottom', // default
    //   color: 'info', // default
    //   icon: 'info',
    //   timeout: 4000, // default
    //   dismissable: true, // default
    //   autoHeight: false, // default
    //   multiLine: false, // default
    //   vertical: false, // default
    //   queueable: true, // default: false
    //   shorts: {
    //     custom: {
    //       color: 'purple',
    //     },
    //   },
    //   property: '$toast', // default
    // });


    // https://github.com/egoist/vue-timeago
    Vue.use(VueTimeago, {
      name: 'Timeago', // Component name, `Timeago` by default
      locale: 'en', // Default locale
      // We use `date-fns` under the hood
      // So you can use all locales from it
      locales: {
        // 'zh-CN': require('date-fns/locale/zh_cn'),
        // ja: require('date-fns/locale/ja')
      },
    });

    // sync component (always load it)
    // Vue.component('top-menu', TopMenu);

    // async component
    // Vue.component(
    //   'async-webpack-example',
    //   // The `import` function returns a Promise.
    //   () => import('./my-async-component')
    // );
  },
};

export default commonfe;
