/* eslint-disable no-param-reassign */

const mutations = {
  // Indiates if service worker got an update
  setNewUpdate(state: any, worker: any) {
    state.hasUpdate = true;
    state.newWorker = worker;
  },
  setNoUpdate(state: any) {
    state.hasUpdate = false;
    state.newWorker = undefined;
  },
  setMenu(state: any, menu: object) {
    state.menu = menu;
  },
  setUploads(state: any, uploads: object) {
    state.uploads = uploads;
  },

  setCurrentTree(state: any, tree: any) {
    state.tree = tree;
  },

  setRootTree(state: any, tree: any) {
    state.rootTree = tree;
  },

  saveURL(state: any, url: string) {
    state.savedURL = url;
  },
};

export default {
  // namespaced: true,
  state: {
    hasUpdate: false,
    newWorker: undefined,

    // Files that are going to be uploaded
    // Type: array of File objects
    uploads: [],

    savedURL: '',

    tree: null,
    rootTree: null,

    menu: {
      title: '',
      items: [],
    }
  },
  mutations,
};
