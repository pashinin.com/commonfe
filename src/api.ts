/**
 * @fileOverview Basic API: making queries, etc...
 * @name api.ts
 * @author Sergey Pashinin
 * @license GPL3
 */

import axios from 'axios';
import Response from './response';
axios.defaults.withCredentials = true;


export default class API {
  // Hostname with scheme.
  // Example: http://localhost:9090
  public host: string;

  public POST: any;
  public GET: any;
  public DELETE: any;
  public PATCH: any;
  public PUT: any;

  private axios: any;

  constructor(hostname: string) {
    this.host = hostname;
    // this.host = 'http://localhost:9090'
    // if (BROWSER) {
    //   const hostname = document.location.hostname;
    //   const staging = hostname.includes('-staging.') || hostname === 'canary.pashinin.com';
    //   const onLocalhost = localHosts.includes(document.location.hostname);
    //   if (!onLocalhost) {
    //     this.host = staging ? 'https://api-staging.pashinin.com' : 'https://api.pashinin.com';
    //   }
    // }
    this.axios = axios.create({
      baseURL: this.host,
      timeout: 3000,
      withCredentials: true,
    });
    this.POST = this.axios.post;
    this.GET = this.axios.get;
    this.DELETE = this.axios.delete;
    this.PATCH = this.axios.patch;
    this.PUT = this.axios.put;
  }

  // Returns URL to captcha image
  public captcha() {
    return `${this.host}/captcha`;
  }

  async currentUser() {
    return await this.axios.get('/auth/me');
  }

  saveJWTexpireTime(data) {
    // Save JWT expire time to local storage
    if (typeof (Storage) !== "undefined" && data && data.expire) {
      window.localStorage.setItem("JWT-expire", data.expire);
    } else {
      // Sorry! No Web Storage support..
    }
  }

  async signin(username: string, password: string) {
    const response = new Response(await this.POST('/auth/signin', {
      username,
      password,
    }));
    this.saveJWTexpireTime(response.data);
    return response;
    // } catch (error) {
    //   throw error;
    // }
  }

  async signup(username: string) {
    let response: any;
    try {
      response = await this.POST('/auth/signup', {
        username,
      });
      if ('err' in response.data) {
        throw Error(response.data.err);
      } else {
        return response;
      }
    } catch (error) {
      throw error;
    }
  }

  async logout() {
    return this.POST('/auth/logout2', {});
  }
};

// const api = new API();

export {
  // apiHttpHost,
  // api as API,
  API,
  // APIdict,
};

// export default api as API;
