export default class Response {
  public data: any;
  public correct: boolean;

  constructor(response: any) {
    this.data = response.data;
  }

  get noErrors() {
    return !this.hasErrors;
  }
  get hasErrors() {
    return Boolean(this.data.error) || Boolean(this.data.errors);
  }

}
